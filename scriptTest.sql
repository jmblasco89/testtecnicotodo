CREATE DATABASE [TestTecnico]
GO
USE [TestTecnico]
GO
/****** Object:  Table [dbo].[ToDo]    Script Date: 8/9/2019 8:44:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ToDo](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [nvarchar](max) NOT NULL,
	[Estado] [bit] NOT NULL,
	[Nombre] [nvarchar](max) NOT NULL,
	[TipoAdjunto] [nvarchar](max) NULL,
	[UrlAdjunto] [nvarchar](max) NULL,
 CONSTRAINT [PK_ToDo] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[ToDo] ADD  DEFAULT (N'') FOR [Nombre]
GO
