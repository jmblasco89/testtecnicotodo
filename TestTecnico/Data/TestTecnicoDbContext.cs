﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestTecnico.Models;

namespace TestTecnico.Data
{
    public class TestTecnicoDbContext : DbContext
    {
        public TestTecnicoDbContext(DbContextOptions<TestTecnicoDbContext> options)
            : base(options)
        {
        }
        public DbSet<ToDo> ToDo { get; set; }
    }
}
