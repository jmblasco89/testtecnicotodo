﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.FileProviders;
using TestTecnico.Data;
using TestTecnico.Models;

namespace TestTecnico.Controllers
{
    public class ToDoController : Controller
    {
        private readonly TestTecnicoDbContext _context;
        private readonly IFileProvider fileProvider;
        private readonly IHostingEnvironment hostingEnvironment;

        public ToDoController(TestTecnicoDbContext context, IFileProvider fileprovider, IHostingEnvironment env)
        {
            _context = context;
            fileProvider = fileprovider;
            hostingEnvironment = env;
        }

        public async Task<IActionResult> Index(string filtroId, string filtroDescripcion, int listaEstados)
        {
            ViewData["CurrentFilter"] = filtroDescripcion;

            List<SelectListItem> selectEstados = new List<SelectListItem>();
            selectEstados.Add(new SelectListItem() { Text = "Todas", Value = "0" });
            selectEstados.Add(new SelectListItem() { Text = "Pendientes", Value = "1" });
            selectEstados.Add(new SelectListItem() { Text = "Finalizadas", Value = "2" });

            ViewBag.listaEstados = selectEstados;
            var toDo = from t in _context.ToDo
                           select t;
            if (!String.IsNullOrEmpty(filtroId))
            {
                toDo = toDo.Where(t => t.Id == Convert.ToInt32(filtroId));
            }
            if (!String.IsNullOrEmpty(filtroDescripcion))
            {
                toDo = toDo.Where(t => t.Descripcion.Contains(filtroDescripcion));
            }
            if (listaEstados == 1)
            {
                toDo = toDo.Where(t => t.Estado == false);
            }
            else if(listaEstados == 2)
            {
                toDo = toDo.Where(t => t.Estado == true);
            }
            return View(await toDo.AsNoTracking().ToListAsync());
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var toDo = await _context.ToDo
                .FirstOrDefaultAsync(m => m.Id == id);
            if (toDo == null)
            {
                return NotFound();
            }

            return View(toDo);
        }

        public IActionResult Create()
        {
            var model = new ToDo();
            return PartialView("_AgregarToDo", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Nombre,Descripcion,Estado,TipoAdjunto,UrlAdjunto")] ToDo model, IList<IFormFile> files)
        {
            if (ModelState.IsValid)
            {
                _context.Add(model);
                await _context.SaveChangesAsync();

                var supportedTypesImg = new[] { ".jpg", ".jpeg", ".tiff", ".gif", ".png" };
                var supportedTypesDoc = new[] { ".txt", ".doc", ".docx", ".pdf", ".xls", ".xlsx" };
                
                foreach (IFormFile source in files)
                {
                    if (source != null || source.Length != 0)
                    {
                        FileInfo fi = new FileInfo(source.FileName);

                        if (supportedTypesImg.Contains(fi.Extension))
                        {
                            var newFilename = model.Id + "_" + String.Format("{0:d}",
                                              (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                            var webPath = hostingEnvironment.WebRootPath;
                            var path = Path.Combine("", webPath + @"\ImageFiles\" + newFilename);

                            var pathToSave = @"/imageFiles/" + newFilename;

                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                await source.CopyToAsync(stream);
                            }
                            model.TipoAdjunto = fi.Extension;
                            model.UrlAdjunto = pathToSave;

                            _context.Update(model);
                            await _context.SaveChangesAsync();
                        }
                        else if (supportedTypesDoc.Contains(fi.Extension))
                        {
                            var newFilename = model.Id + "_" + String.Format("{0:d}",
                                              (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                            var webPath = hostingEnvironment.WebRootPath;
                            var path = Path.Combine("", webPath + @"\docFiles\" + newFilename);

                            var pathToSave = @"/docFiles/" + newFilename;

                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                await source.CopyToAsync(stream);
                            }
                            model.TipoAdjunto = fi.Extension;
                            model.UrlAdjunto = pathToSave;
                            _context.Update(model);
                            await _context.SaveChangesAsync();
                        }

                    }
                }                

                return RedirectToAction(nameof(Index));
            }
            return RedirectToAction("Create");
        }

        public async Task<IActionResult> Edit(int? id)
        {
            var supportedTypesImg = new[] { ".jpg", ".jpeg", ".tiff", ".gif", ".png" };
            var supportedTypesDoc = new[] { ".txt", ".doc", ".docx", ".pdf", ".xls", ".xlsx" };

            ViewBag.supportedTypesImg = supportedTypesImg;
            ViewBag.supportedTypesDoc = supportedTypesDoc;
            if (id == null)
            {
                return NotFound();
            }
            var toDo = await _context.ToDo.FindAsync(id);

            if (toDo == null)
            {
                return NotFound();
            }

            return PartialView("_ModificarToDo", toDo);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(ToDo model, IList<IFormFile> files)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var supportedTypesImg = new[] { ".jpg", ".jpeg", ".tiff", ".gif", ".png" };
                    var supportedTypesDoc = new[] { ".txt", ".doc", ".docx", ".pdf", ".xls", ".xlsx" };

                    foreach (IFormFile source in files)
                    {
                        if (source != null || source.Length != 0)
                        {
                            FileInfo fi = new FileInfo(source.FileName);

                            if (supportedTypesImg.Contains(fi.Extension))
                            {
                                var newFilename = model.Id + "_" + String.Format("{0:d}",
                                                  (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                                var webPath = hostingEnvironment.WebRootPath;
                                var path = Path.Combine("", webPath + @"\ImageFiles\" + newFilename);

                                var pathToSave = @"/imageFiles/" + newFilename;

                                using (var stream = new FileStream(path, FileMode.Create))
                                {
                                    await source.CopyToAsync(stream);
                                }
                                model.TipoAdjunto = fi.Extension;
                                model.UrlAdjunto = pathToSave;

                                _context.Update(model);
                                await _context.SaveChangesAsync();
                            }
                            else if (supportedTypesImg.Contains(fi.Extension))
                            {
                                var newFilename = model.Id + "_" + String.Format("{0:d}",
                                                  (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                                var webPath = hostingEnvironment.WebRootPath;
                                var path = Path.Combine("", webPath + @"\docFiles\" + newFilename);

                                var pathToSave = @"/docFiles/" + newFilename;

                                using (var stream = new FileStream(path, FileMode.Create))
                                {
                                    await source.CopyToAsync(stream);
                                }
                                model.TipoAdjunto = fi.Extension;
                                model.UrlAdjunto = pathToSave;
                                _context.Update(model);
                                await _context.SaveChangesAsync();
                            }

                        }
                    }
                    _context.Update(model);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ToDoExists(model.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(model);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var toDo = await _context.ToDo
                .FirstOrDefaultAsync(m => m.Id == id);
            if (toDo == null)
            {
                return NotFound();
            }

            return View(toDo);
        }

        // POST: ToDo/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var toDo = await _context.ToDo.FindAsync(id);
            _context.ToDo.Remove(toDo);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ToDoExists(int id)
        {
            return _context.ToDo.Any(e => e.Id == id);
        }
    }
}
