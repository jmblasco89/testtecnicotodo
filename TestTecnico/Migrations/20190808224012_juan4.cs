﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TestTecnico.Migrations
{
    public partial class juan4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Adjunto");

            migrationBuilder.AddColumn<string>(
                name: "TipoAdjunto",
                table: "ToDo",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UrlAdjunto",
                table: "ToDo",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TipoAdjunto",
                table: "ToDo");

            migrationBuilder.DropColumn(
                name: "UrlAdjunto",
                table: "ToDo");

            migrationBuilder.CreateTable(
                name: "Adjunto",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    TipoAdjunto = table.Column<string>(nullable: true),
                    ToDoId = table.Column<int>(nullable: true),
                    UrlAdjunto = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Adjunto", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Adjunto_ToDo_ToDoId",
                        column: x => x.ToDoId,
                        principalTable: "ToDo",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Adjunto_ToDoId",
                table: "Adjunto",
                column: "ToDoId");
        }
    }
}
