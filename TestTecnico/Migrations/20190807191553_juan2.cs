﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TestTecnico.Migrations
{
    public partial class juan2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Descripcion",
                table: "ToDo",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Nombre",
                table: "ToDo",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Nombre",
                table: "ToDo");

            migrationBuilder.AlterColumn<string>(
                name: "Descripcion",
                table: "ToDo",
                nullable: true,
                oldClrType: typeof(string));
        }
    }
}
