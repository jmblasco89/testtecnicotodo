﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TestTecnico.Migrations
{
    public partial class juan3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "TipoAdjunto",
                table: "Adjunto",
                nullable: true,
                oldClrType: typeof(int));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "TipoAdjunto",
                table: "Adjunto",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
