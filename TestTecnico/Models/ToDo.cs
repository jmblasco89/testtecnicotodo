﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TestTecnico.Models
{
    public class ToDo
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Nombre { get; set; }
        [Required]
        [Display(Name = "Descripción")]
        public string Descripcion { get; set; }
        [Display(Name = "Tarea Lista?")]
        public bool Estado { get; set; }
        public string TipoAdjunto { get; set; }
        public string UrlAdjunto { get; set; }        
    }
}
